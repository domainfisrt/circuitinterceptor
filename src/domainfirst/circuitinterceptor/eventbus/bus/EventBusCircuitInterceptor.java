package domainfirst.circuitinterceptor.eventbus.bus;

import domainfirst.circuitinterceptor.eventbus.event.Event;
import domainfirst.circuitinterceptor.eventbus.interceptor.EventInterceptor;
import domainfirst.circuitinterceptor.eventbus.interceptor.EventInterceptorChain;

class EventBusCircuitInterceptor implements EventInterceptor 
{
	private final EventBusImpl eventBus;
	
	public EventBusCircuitInterceptor(final EventBusImpl eventBus) 
	{
		this.eventBus = eventBus;
	}

	@Override
	public void broadcastEvent(Event event, EventInterceptorChain chain) 
	{
		this.eventBus.sendEventToListeners(event);
	}
}