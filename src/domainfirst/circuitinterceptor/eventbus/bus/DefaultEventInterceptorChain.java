package domainfirst.circuitinterceptor.eventbus.bus;

import java.util.ArrayList;
import java.util.List;

import domainfirst.circuitinterceptor.eventbus.event.Event;
import domainfirst.circuitinterceptor.eventbus.interceptor.EventInterceptor;
import domainfirst.circuitinterceptor.eventbus.interceptor.EventInterceptorChain;

class DefaultEventInterceptorChain implements EventInterceptorChain 
{
	private final List<EventInterceptor> eventInterceptors;
	
	private int index;
	
	public DefaultEventInterceptorChain(final List<EventInterceptor> eventInterceptors)
	{
		this.eventInterceptors = new ArrayList<>(eventInterceptors);
		this.index = 0;
	}

	@Override
	public void broadcastEvent(Event event) 
	{
		if( index <= (eventInterceptors.size() -1)   )
		{
			this.eventInterceptors.get(index).broadcastEvent(event, this);
			this.index++;
		}
	}
}