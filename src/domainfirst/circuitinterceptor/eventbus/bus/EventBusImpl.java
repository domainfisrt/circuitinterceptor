package domainfirst.circuitinterceptor.eventbus.bus;

import java.util.ArrayList;
import java.util.List;

import domainfirst.circuitinterceptor.eventbus.event.Event;
import domainfirst.circuitinterceptor.eventbus.interceptor.EventInterceptor;
import domainfirst.circuitinterceptor.eventbus.listener.EventListener;

public class EventBusImpl implements EventBus
{
	private final List<EventListener> eventListeners;
	
	private final List<EventInterceptor> eventInterceptors;

	public EventBusImpl(final List<EventInterceptor> eventInterceptors)
	{
		this.eventListeners = new ArrayList<>();
		this.eventInterceptors = new ArrayList<>(eventInterceptors);
	}
	
	@Override
	public void registerListener(EventListener listener) 
	{
		this.eventListeners.add(listener);
	}

	@Override
	public void unregisterListener(EventListener listener) 
	{
		this.eventListeners.remove(listener);
	}

	@Override
	public void broadcastEvent(Event event) 
	{
		final List<EventInterceptor> inteceptors = new ArrayList<>(this.eventInterceptors);
		
		inteceptors.add(new EventBusCircuitInterceptor(this));
		
		new DefaultEventInterceptorChain(inteceptors).broadcastEvent(event);
	}
	
	protected void sendEventToListeners(Event event)
	{
		this.eventListeners.stream().forEach(listener -> listener.handleEvent(event));
	}
}