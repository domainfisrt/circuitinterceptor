package domainfirst.circuitinterceptor.eventbus.bus;

import domainfirst.circuitinterceptor.eventbus.event.Event;
import domainfirst.circuitinterceptor.eventbus.listener.EventListener;

public interface EventBus
{
	void registerListener(EventListener listener);
	
	void unregisterListener(EventListener listener);
	
	void broadcastEvent(Event event);
}