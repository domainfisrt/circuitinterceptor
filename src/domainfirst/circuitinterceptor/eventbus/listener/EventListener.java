package domainfirst.circuitinterceptor.eventbus.listener;

import domainfirst.circuitinterceptor.eventbus.event.Event;

public interface EventListener
{
	void handleEvent(Event Event);
}