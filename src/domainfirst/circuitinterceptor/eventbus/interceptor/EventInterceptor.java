package domainfirst.circuitinterceptor.eventbus.interceptor;

import domainfirst.circuitinterceptor.eventbus.event.Event;

public interface EventInterceptor 
{
	void broadcastEvent(Event event, EventInterceptorChain chain);
}