package domainfirst.circuitinterceptor.eventbus.interceptor;

import domainfirst.circuitinterceptor.eventbus.event.Event;

public interface EventInterceptorChain
{
	void broadcastEvent(Event event);
}